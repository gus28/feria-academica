package com.example.covidprueba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Comenzar extends AppCompatActivity {

    private Button comenzar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comenzar);

        getSupportActionBar().hide();

        comenzar = (Button)findViewById(R.id.btn_comenzar);

        comenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Comenzar.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}